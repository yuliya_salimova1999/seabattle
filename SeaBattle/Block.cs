﻿namespace SeaBattle
{
    class Block
    {
        public int X { get; }
        public int Y { get; }
        public string Status { get; set;}
        public Block(int x, int y)
        {
            X = x;
            Y = y;
            Status = "free";
        }
    }
}
