﻿using System;
using System.Collections.Generic;

namespace SeaBattle
{
    class Field
    {
        public Random Random { get; }
        public Block[,] BattleField { get; }
        public int Length { get; }
        public int Hight { get; }
        public int DamagedBlocksCount { get; set; }
        public List<ShipStatistics> Statistics { get; }
        public Field(int x, int y)
        {
            BattleField = new Block[x, y];
            Length = x;
            Hight = y;
            Random = new Random();
            Statistics = new List<ShipStatistics>();
            for (int row = 0; row < x; row++)
            {
                for (int column = 0; column < y; column++)
                {
                    BattleField[row, column] = new Block(row, column);
                }
            }
        }
        public void CreateRandomField()
        {
            int count = 0;
            Ship ship;
            ship = new FourBlocksShip(FindFreeBlock());
            while (true)
            {
                if (ship.CreateShip(this)) break;
            }
            while (count != 2)
            {
                ship = new ThreeBlocksShip(FindFreeBlock());
                while (true)
                {
                    if (ship.CreateShip(this)) break;
                }
                count++;
            }
            count = 0;
            while (count != 3)
            {
                ship = new TwoBlocksShip(FindFreeBlock());
                while (true)
                {
                    if (ship.CreateShip(this)) break;
                }
                count++;
            }
            count = 0;
            while (count != 4)
            {
                ship = new OneBlockShip(FindFreeBlock());
                while (true)
                {
                    if (ship.CreateShip(this)) break;
                }
                count++;
            }
        }
        public bool Shoot(Block block)
        {
            if (BattleField[block.X, block.Y].Status == "*")
            {
                return true;
            }
            if (BattleField[block.X, block.Y].Status == "0")
            {
                BattleField[block.X, block.Y].Status = "x";
                foreach (ShipStatistics shipStatistics in Statistics)
                {
                    if (shipStatistics.Ship.OccupiedBlocks.Contains(BattleField[block.X, block.Y]))
                    {
                        shipStatistics.Ship.OccupiedBlocks.Remove(BattleField[block.X, block.Y]);
                        shipStatistics.State = "damaged";
                        if (shipStatistics.Ship.OccupiedBlocks.Count == 0)
                        {
                            shipStatistics.State = "killed";
                        }
                    }
                }
                DamagedBlocksCount++;
                return true;
            }
            BattleField[block.X, block.Y].Status = "*";
            return false;
        }
        public void Draw()
        {
            Console.WriteLine("  0123456789");
            for (int row = 0; row < Length; row++)
            {
                Console.Write(row + " ");
                for (int col = 0; col < Hight; col++)
                {
                    if (BattleField[row, col].Status == "free")
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write(BattleField[row, col].Status);
                    }
                }
                Console.Write("\n");
            }
        }
        private Block FindFreeBlock()
        {
            Block block = BattleField[Random.Next(0, Length - 1), Random.Next(0, Hight - 1)];
            if (block.Status != "free")
            {
                return FindFreeBlock();
            }
            return block;
        }
    }
}