﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class Program
    {
        static void Main(string[] args)
        {
            Player user = new Player();
            Player computer = new Player();
            user.FieldWithShips.CreateRandomField();
            computer.FieldWithShips.CreateRandomField();
            user.FieldWithShips.Draw();
            while (true)
            {
                if (user.FieldWithShips.DamagedBlocksCount == 20)
                {
                    Console.WriteLine("Looooooseeeeer!");
                    break;
                }
                else if (computer.FieldWithShips.DamagedBlocksCount == 20)
                {
                    Console.WriteLine("You're a killing machine!");
                    break;
                }
                Console.WriteLine("1. Show statistics    2. Shoot");
                string option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        user.ShowStatistics();
                        break;
                    case "2":
                        user.Shoot(computer);
                        break;
                }
                computer.RandomShoot(user);
            }
            Console.ReadKey();
        }
    }
}