﻿using System;
using System.Text.Json;
namespace SeaBattle
{
    class Player
    {
        private Random random = new Random();
        public Field FieldWithShips { get; }
        public Field FieldToShoot { get; }
        public Player()
        {
            FieldWithShips = new Field(10, 10);
            FieldToShoot = new Field(10, 10);
        }
        public void Shoot(Player player)
        {
            while (true)
            {
                Console.WriteLine("Enter the coordinates: format XY");
                string input = Console.ReadLine();
                if (!player.FieldWithShips.Shoot(new Block(int.Parse(input[0].ToString()), int.Parse(input[1].ToString()))))
                {
                    FieldToShoot.BattleField[int.Parse(input[0].ToString()), int.Parse(input[1].ToString())].Status = "*";
                    break;
                }
                FieldToShoot.BattleField[int.Parse(input[0].ToString()), int.Parse(input[1].ToString())].Status = "x";
            }
            FieldToShoot.Draw();
        }
        public void RandomShoot(Player player)
        {
            while (true)
            {
                Block block = new Block(random.Next(0, FieldToShoot.Length - 1), random.Next(0, FieldToShoot.Hight - 1));
                if (!player.FieldWithShips.Shoot(block))
                {
                    FieldToShoot.BattleField[block.X, block.Y].Status = "*";
                    break;
                }
                else if (FieldToShoot.BattleField[block.X, block.Y].Status == "*" && player.FieldWithShips.Shoot(block))
                {
                    continue;
                }
                FieldToShoot.BattleField[block.X, block.Y].Status = "x";
            }
            player.FieldWithShips.Draw();
        }
        public void ShowStatistics()
        {
            string json = "";
            foreach (ShipStatistics statistics in FieldWithShips.Statistics)
            {
                json += JsonSerializer.Serialize(statistics)+"\n\n";
            }
            Console.WriteLine(json);
        }
    }
}
