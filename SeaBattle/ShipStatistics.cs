﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class ShipStatistics
    {
        public Ship Ship { get; }
        public string State { get; set; }
        public ShipStatistics(Ship ship, string state)
        {
            Ship = ship;
            State = state;
        }
    }
}
