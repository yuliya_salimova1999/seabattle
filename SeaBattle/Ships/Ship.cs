﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    abstract class Ship
    {
        protected Block startBlock;
        public List<ShiftParametersSet> ParametersSets { get; set; }
        public List<Block> OccupiedBlocks { get; set; }
        public bool CreateShip(Field field)
        {
            List<Block> blocksToOccupy = new List<Block>();
            blocksToOccupy.Add(startBlock);
            ShiftParametersSet shiftParameters = ParametersSets[field.Random.Next(0, ParametersSets.Count-1)];
            if (shiftParameters != null)
            {
                foreach (ShiftParameter parametr in shiftParameters.ShiftParameters)
                {
                   blocksToOccupy.Add(new Block(startBlock.X + parametr.XParameter, startBlock.Y + parametr.YParameter));
                }
            }
            if (!IsAreaFree(blocksToOccupy, field)) return false;
            OccupyBlocks(blocksToOccupy, field);
            field.Statistics.Add(new ShipStatistics(this, "alive"));
            return true;
        }
        private void OccupyBlocks(List<Block> blocks, Field field)
        {
            foreach (Block block in blocks)
            {
                for (int x = block.X - 1; x <= block.X + 1; x++)
                {
                    for (int y = block.Y - 1; y <= block.Y + 1; y++)
                    {
                        if ((x < 0 || x >= field.Length) ||
                            (y < 0 || y >= field.Hight))
                            continue;
                        if (field.BattleField[x, y].Status == "free") field.BattleField[x, y].Status = "-";
                    }
                }
                field.BattleField[block.X, block.Y].Status = "0";
                OccupiedBlocks.Add(field.BattleField[block.X, block.Y]);
            }
        }
        private bool IsAreaFree(List<Block> blocksToOccupy, Field field)
        {
            foreach (Block block in blocksToOccupy)
            {
                if (block.X >= field.Length || block.Y >= field.Hight || field.BattleField[block.X, block.Y].Status != "free") return false;
            }
            return true;
        }
    }
}
