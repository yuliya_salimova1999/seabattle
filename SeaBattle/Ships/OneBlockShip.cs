﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class OneBlockShip : Ship
    {
        public OneBlockShip(Block startBlock)
        {
            OccupiedBlocks = new List<Block>();
            ParametersSets = new List<ShiftParametersSet>();
            ParametersSets.Add(null);
            this.startBlock = startBlock;
        }
    }
}
