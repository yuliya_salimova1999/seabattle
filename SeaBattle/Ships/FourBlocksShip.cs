﻿using System.Collections.Generic;

namespace SeaBattle
{
    class FourBlocksShip : Ship
    {
        public FourBlocksShip(Block startBlock)
        {
            OccupiedBlocks = new List<Block>();
            ParametersSets = new List<ShiftParametersSet>();
            this.startBlock = startBlock;
            ParametersSets.Add(new ShiftParametersSet(new List<ShiftParameter>() { new ShiftParameter(1, 0),
                new ShiftParameter(2, 0), new ShiftParameter(3, 0) }));
            ParametersSets.Add(new ShiftParametersSet(new List<ShiftParameter>() { new ShiftParameter(0, 1),
                new ShiftParameter(0, 2), new ShiftParameter(0, 3) }));
            ParametersSets.Add(new ShiftParametersSet(new List<ShiftParameter>() { new ShiftParameter(0, 1),
                new ShiftParameter(1, 0), new ShiftParameter(1, 1) }));
            ParametersSets.Add(new ShiftParametersSet(new List<ShiftParameter>() { new ShiftParameter(0, 1),
                new ShiftParameter(1, 1), new ShiftParameter(1, 2) }));
            ParametersSets.Add(new ShiftParametersSet(new List<ShiftParameter>() { new ShiftParameter(0, 1),
                new ShiftParameter(-1, 1), new ShiftParameter(-1, 2) }));
            ParametersSets.Add(new ShiftParametersSet(new List<ShiftParameter>() { new ShiftParameter(1, 0),
                new ShiftParameter(1, 1), new ShiftParameter(2, 1) }));
            ParametersSets.Add(new ShiftParametersSet(new List<ShiftParameter>() { new ShiftParameter(-1, 0),
                new ShiftParameter(-1, 1), new ShiftParameter(-2, 1) }));
        }
    }
}