﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class TwoBlocksShip : Ship
    {
        public TwoBlocksShip(Block startBlock)
        {
            OccupiedBlocks = new List<Block>();
            ParametersSets = new List<ShiftParametersSet>();
            this.startBlock = startBlock;
            ParametersSets.Add(new ShiftParametersSet( new List<ShiftParameter> { new ShiftParameter(1, 0) }));
            ParametersSets.Add(new ShiftParametersSet(new List<ShiftParameter> { new ShiftParameter(0, 1) }));
        }
    }
}
