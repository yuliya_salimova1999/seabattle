﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class ShiftParameter
    {
        public int XParameter { get; }
        public int YParameter { get; }
        public ShiftParameter(int x, int y)
        {
            XParameter = x;
            YParameter = y;
        }
    }
}
