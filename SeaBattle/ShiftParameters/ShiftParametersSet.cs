﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class ShiftParametersSet
    {
        public List<ShiftParameter> ShiftParameters { get; }
        public ShiftParametersSet(List<ShiftParameter> shiftParameters)
        {
            ShiftParameters = shiftParameters;
        }
    }
}
